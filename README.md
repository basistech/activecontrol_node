[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://bitbucket.org/basistech/activecontrol_node)

# ActiveControl Node

A collection of modules for accessing [ActiveControl](https://www.basistechnologies.com/products/activecontrol/) APIs from nodejs and [Node-Red](https://nodered.org)

- [ActiveControl SOAP API](./packages/activecontrol-soap-api/README.md)
- [ActiveControl SOAP Node-RED](./packages/node-red-contrib-activecontrol/README.md)
- [ActiveControl Node-RED Custom Storage Plugin](./packages/node-red-contrib-storage-bti/README.md)

[TOC]

# Setting up the dev environment on Windows
## Prerequisites

1. [Visual Studio Code](https://code.visualstudio.com/)
1. [docker](https://www.docker.com/products/docker-desktop)
1. [git](https://git-scm.com/)
1. [Remote container extension for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
1. [Remote WSL extension for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)
1. [Ubuntu distribution for Win10](https://www.microsoft.com/store/productId/9NBLGGH4MSV6)
1. Create an SSH key pair using ssh-keygen. You should have an id_rsa and id_rsa.pub.
1. Recommended extensions for VS Code: Git Graph, Docker

## Installation instructions
### Why do we need all of these steps?
This docker container contains all the development tools needed for developing with node-red. Once set up, VS Code connects to the container and you work in the container. Since the container can be rebuilt at any time, it should not contain any project files (ie. git repositories). To "connect" the container to the project files, the project directory needs to be mounted into the container.

If the project directory resides in your Windows file system (you can navigate there with the Explorer), the container performance will be TERRIBLE ([more info here](https://code.visualstudio.com/docs/remote/containers-advanced#_store-your-source-code-in-the-wsl-2-filesystem-on-windows)). This is why we need to setup a Linux subsystem (WSL) and store the project directory in that file system. A WSL distribution is like a small OS inside your host OS Windows.

### Steps
- Install VS Code and all of the necessary extensions. Also install git.
- If the WSL2 feature is not enabled in your Windows, enable it in the Windows Features. Install the Ubuntu distro and make sure that WSL 2 is enabled, not 1. (There are some powershell commands to do that).
- Install docker and make sure that it uses WSL2, not Hyper-V. Go to `Settings->Resources->WSL integration` and check "Enable integration with my default WSL distro" and also check "Enable integration with additional distros: Ubuntu".
- Open the Ubuntu terminal by clicking on the app icon. This is your gateway to the Linux subsystem. Type "`code .`" to start VS Code. It's easier to work with this UI. Start a terminal in VS Code.
- Configure SSH authentication between Ubuntu and bitbucket so that you can clone the git repository. You need to generate a key pair and upload the public key on bitbucket. Copy your `id_rsa` file into a _new_ directory `~/.ssh` by creating the directory in VS Code (or using the terminal) and dragging and dropping the file. Run this command to check that SSH works: `ssh -T git@bitbucket.org`
- Now create a folder `git-workspace` (or whatever name you wish). Open this folder by typing `cd git-workspace`.
- Clone the dev container repository into that folder via
  ```bash
  git clone git@bitbucket.org:basistech/activecontrol_node.git
  ```
- Open this folder. VS Code will prompt you to open the folder as a container, but don't do that yet.
- Copy your `id_rsa` file into the _existing_ folder `.ssh` by dragging and dropping.
- Now follow the prompt to open this folder in a container.
- Follow another prompt to open it as a workspace.
- Ensure that SSH from the container to bitbucket is working by running `ssh -T git@bitbucket.org`
- Done! You can now develop in the activecontrol_node repository and push and pull to bitbucket. When you rebuild or delete the docker container, the project repository will survive untouched in your Ubuntu WSL subsystem.

# Node-RED modules
> Please create a different folder for each module

All nodes and other libraries will be stored in this monorepository as we don't expect to change them often.

## Installing a clean copy of the repository
- Pull the latest changes
- Delete all `node_modules` and `dist` folders (helpful commands: `npm clean` and `npx lerna clean`)
- Run the `version-management-wizard` script and select `bootstrap`

## Starting the node-RED server
- Run the `run-nodered` script
- Once started, the server will be available as [http://127.0.0.1:1881](http://127.0.0.1:1881)

### Debugging
- Easiest option is to just press **F5**
- You can now set breakpoints in the nodes you're developing. No hot reloading, after updating a module you will have to restart the server/debugger.

## Development
Can be done on any computer supporting nodejs, need to install node (12.x I think), node-red and the support software.
Dev containers are easier IMHO. See here to [run locally](https://nodered.org/docs/getting-started/local).

### Folders
- [/workspaces/activecontrol_node](./) is the main project folder
- [/workspaces/activecontrol_node/packages](./packages) contains all packages, i.e. node-red and libraries.
- [/workspaces/activecontrol_node/packages/"nodered-module"/examples](./packages/node-red-contrib-activecontrol/examples) contains example flows

## Installing a new node in the container
- Publicly available nodes can be installed from the node-red UI
- To install a node under development, install the development folder in the nodered installation. This will make your node available by taking a snapshot of the existing code. Whenever you make a change to test it you will need to repeat the installation:

```bash
# repeat every time you want to update the installed version
> cd
> npm i /workspaces/activecontrol_node/packages/new_node
```

> The new node will be available after restarting node-red.

## Creating a new module
- Run the `version-management-wizard` to create a new package
- Navigate to the package and run `yo nodered-ts`
- Run `code -a .`
export const errorText = (
  error: any,
  defaultmsg = "An unknown error occured"
) => (error && (error.message || error.toString())) || defaultmsg

export const waitmillis = (t: number) =>
  new Promise(resolve => setTimeout(resolve, t))
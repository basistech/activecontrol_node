import { NodeAPI } from "node-red"
import {
  isReadTaskMessage,
  ReadTaskNode,
  ReadTaskNodeDef
} from "../readTaskDefinitions"
import {
  mergeOptions,
  ActiveControlSoapClient
} from "@basistechnologies/activecontrol-soap-api"
import { AcConfigNode } from "../ac_config_definitions"
import { errorText } from "./util"

module.exports = (RED: NodeAPI) => {
  function createReadTaskNode(this: ReadTaskNode, nodeDef: ReadTaskNodeDef) {
    const node = this

    // Create node based on the passed instance definition
    RED.nodes.createNode(node, nodeDef)

    // Get the AC configuration and apply it
    node.activecontrol = RED.nodes.getNode(
      nodeDef.activecontrol
    ) as AcConfigNode

    // check if a config node is given and valid
    if (!node.activecontrol) throw new Error("Configuration node missing")
    if (!node.activecontrol.config || !node.activecontrol.credentials)
      throw new Error("Configuration node not set up properly")

    node.on("input", async function (msg, send, done) {
      try {
        node.status({ fill: "blue", text: "Reading task details..." })

        if (!isReadTaskMessage(msg))
          throw new Error("Message format not recognized")

        // Check if options were overridden in this node and merge them
        const { url, password, systemnumber, username } = msg
        const config = {
          ...node.activecontrol!.config!,
          ...node.activecontrol!.credentials
        }
        const options = mergeOptions(config, {
          url,
          password,
          systemnumber,
          username
        })

        // Make the call
        const taskReader = new ActiveControlSoapClient(options)
        const task = await taskReader.readTask(msg.payload)

        send({ ...msg, payload: task })
        node.status("")
        done()
      } catch (error) {
        node.status({ fill: "red", text: errorText(error) })
        done(error)
      }
    })
  }
  RED.nodes.registerType("read-task", createReadTaskNode)
}

import { NodeAPI, Node, NodeDef } from "node-red"

export interface Credentials {
  password: string
  authorization: string
  username: string
}

export interface ACConfig {
  url: string
  systemnumber: string
}
export interface ACConfigDef extends NodeDef, ACConfig {}
export interface AcConfigNode extends Node<Credentials> {
  config?: ACConfig
}

export const registerAcConfig = (RED: NodeAPI): void => {
  function createNodeInstance(this: AcConfigNode, config: ACConfigDef) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const { url, systemnumber } = config
    this.config = { url, systemnumber }
    RED.nodes.createNode(this, config)
  }
  // register the node handler
  RED.nodes.registerType("ac_config", createNodeInstance, {
    credentials: {
      // should match ac_config_node.html
      authorization: { type: "password" },
      password: { type: "password" },
      username: { type: "text" }
    }
  })
}
export default registerAcConfig

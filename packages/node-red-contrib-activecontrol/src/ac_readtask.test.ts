import { isTask } from "@basistechnologies/activecontrol-soap-api"
import { config } from "dotenv"
import helper from "node-red-node-test-helper"
import { env } from "process"
import {
  ReadTaskNode
} from "./readTaskDefinitions"
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ac_config = require("./nodes/ac_config_node")
const readtask = require("./nodes/readTaskNode")

config()
const readConfigFromEnv = () => ({
  url: env.AC_SERVER || "",
  username: env.AC_USER,
  password: env.AC_PASSWORD,
  systemnumber: env.AC_SYSTEM_NUMBER || ""
})

beforeEach(done => helper.startServer(done))
afterEach(done => {
  helper.unload()
  helper.stopServer(done)
})

test("node registration", done => {
  const flow = [
    {
      id: "read",
      type: "read-task",
      name: "readTask",
      activecontrol: "ac",
      wires: [["n2"]]
    },
    { id: "ac", type: "ac_config", name: "test name", url: "http://foo" },
    { id: "n2", type: "helper" }
  ]
  helper.load([ac_config, readtask], flow, function () {
    const n2 = helper.getNode("read") as ReadTaskNode

    expect(n2).toBeDefined()
    expect(n2.name).toBe("readTask")
    expect(n2.activecontrol?.id).toBe("ac")
    expect(n2.activecontrol?.config?.url).toBe("http://foo")
    done()
  })
})

test("node reads a task", async done => {
  const { url, systemnumber, username, password } = readConfigFromEnv()
  const flow = [
    {
      id: "read",
      type: "read-task",
      name: "readTask",
      activecontrol: "ac",
      wires: [["n2"]]
    },
    {
      id: "ac",
      type: "ac_config",
      name: "test name",
      url,
      systemnumber
    },
    { id: "n2", type: "helper" }
  ]

  const result: any = await new Promise(resolve => {
    helper.load(
      [ac_config, readtask],
      flow,
      { ac: { username, password } },
      function () {
        const reader = helper.getNode("read") as ReadTaskNode
        const receiver = helper.getNode("n2") as ReadTaskNode

        expect(reader).toBeDefined()
        expect(reader.name).toBe("readTask")
        receiver.on("input", function (msg) {
          resolve(msg)
        })
        reader.receive({ payload: { XTaskid: env.AC_TASKID } })
      }
    )
  })
  const task = result?.payload
  if (!isTask(task)) fail("I expected a task")
  expect(task.ID).toBe(env.AC_TASKID)
  done()
})



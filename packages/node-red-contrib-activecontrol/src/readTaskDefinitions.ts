import {
  Options,
  ReadTaskInputs
} from "@basistechnologies/activecontrol-soap-api"
import { NodeDef, Node, NodeMessage } from "node-red"
import { AcConfigNode } from "./ac_config_definitions"

export interface ReadTaskNodeDef extends NodeDef {
  activecontrol: string
}

export interface ReadTaskNode extends Node {
  activecontrol?: AcConfigNode
}

export interface ReadTaskNodeMessage extends NodeMessage, Partial<Options> {
  payload: ReadTaskInputs
}

export function isReadTaskMessage(
  msg: NodeMessage
): msg is ReadTaskNodeMessage {
  return !!(msg as any)?.payload?.XTaskid
}


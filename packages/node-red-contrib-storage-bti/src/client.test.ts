import { config } from "dotenv"
import { env } from "process"
import { Client, Settings } from "./client"
import { AxiosResponse } from "axios"

config()
const readConfigFromEnv = (): Settings => ({
  storageBaseUrl: env.AC_SERVER?.match(/^.*:\d+/)?.shift() || "", // strip server URL
  storageUsername: env.AC_USER,
  storagePassword: env.AC_PASSWORD
})

test("Basic call of root API", async () => {
  const client = new Client(readConfigFromEnv())

  return client
    .performGetRequest("")
    .then(result => {
      expect(result).toBeTruthy()
      expect((result as AxiosResponse).status).toBe(200)
    })
    .catch(error => {
      fail(error)
    })
})

test("GET with invalid resource", async () => {
  const client = new Client(readConfigFromEnv())

  return client
    .performGetRequest("/where/is/this")
    .then(result => {
      expect(result).toBeNull()
    })
    .catch(error => {
      fail(error)
    })
})
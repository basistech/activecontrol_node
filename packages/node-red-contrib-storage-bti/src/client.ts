import axios, { AxiosResponse } from "axios"

export type Settings = {
  storageBaseUrl: string
  storageUsername?: string
  storagePassword?: string
  storageAuthorization?: string
}

export class Client {
  readonly settings: Settings

  constructor(nodeRedSettings: Partial<Settings>) {
    this.validateSettings(nodeRedSettings)
    let httpAuth

    if (nodeRedSettings.storageAuthorization) {
      httpAuth = nodeRedSettings.storageAuthorization
    } else {
      httpAuth = Buffer.from(
        `${nodeRedSettings.storageUsername}:${nodeRedSettings.storagePassword}`
      ).toString("base64")
      httpAuth = `Basic ${httpAuth}`
    }

    this.settings = { ...nodeRedSettings, storageAuthorization: httpAuth }

    // append web service postfix
    if (this.settings.storageBaseUrl.endsWith("/"))
      this.settings.storageBaseUrl += "bti/nodered"
    else this.settings.storageBaseUrl += "/bti/nodered"
  }

  public async performGetRequest(
    urlPath: string
  ): Promise<AxiosResponse | null> {
    // will NOT throw an error, returns null instead
    return axios(this.settings.storageBaseUrl + urlPath, {
      method: "get",
      headers: this.requestHeader()
    })
      .then((response: any) => (response ? response : null))
      .catch(() => null)
  }

  public async performPutRequest(
    urlPath: string,
    content: object
  ): Promise<AxiosResponse> {
    return Promise.resolve(content)
      .then(JSON.stringify)
      .then(json =>
        axios.put(this.settings.storageBaseUrl + urlPath, json, {
          method: "post",
          headers: this.requestHeader(true)
        })
      )
      .then(response => {
        if (response.status < 200 || response.status > 299)
          this.throwLongError(response)

        return response
      })
      .catch(error => {
        throw new Error("Request failed: " + error.message)
      })
  }

  public async performDeleteRequest(urlPath: string): Promise<AxiosResponse> {
    return axios(this.settings.storageBaseUrl + urlPath, {
      method: "delete",
      headers: this.requestHeader()
    })
      .then(response => {
        if (response.status < 200 || response.status > 299)
          this.throwLongError(response)

        return response
      })
      .catch(error => {
        throw new Error("Request failed: " + error.message)
      })
  }

  private requestHeader = (hasContent?: boolean) => ({
    "Content-Type": "text/xml;charset=UTF-8",
    Authorization: this.settings.storageAuthorization,
    ...(hasContent && {
      Accept: "application/json",
      "Content-Type": "application/json"
    })
  })

  private throwLongError(response: AxiosResponse): never {
    throw new Error(
      "Server responded with " +
        response.status +
        ", status text:" +
        response.statusText
    )
  }

  private validateSettings(
    settings: Partial<Settings>
  ): asserts settings is Settings {
    if (!settings.storageBaseUrl?.match(/^https?:\/\//i))
      throw new Error(`Invalid AC URL provided: ${settings.storageBaseUrl}`)
    if (
      !settings.storageAuthorization &&
      !(settings.storageUsername && settings.storagePassword)
    )
      throw new Error(
        "No authentication for the Storage API provided in the configuration"
      )
  }
}

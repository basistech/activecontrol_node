import { ReadTaskTransportsInputs } from ".."
import { createClient, readEnv } from "./utils"

test("Read existing task transports", async () => {
  const client = createClient()

  const payload: ReadTaskTransportsInputs = {
    XTaskReference: readEnv("AC_TASK_REFERENCE")
  }

  const taskTransports = await client.readTaskTransports(payload)
  expect(Array.isArray(taskTransports.taskIds)).toBe(true)
  expect(taskTransports.taskIds).toHaveLength(1)
  expect(taskTransports.taskIds[0].Id).toBe(readEnv("AC_TASKID"))

  expect(Array.isArray(taskTransports.transports)).toBe(true)
  expect(taskTransports.transports.length).toBeGreaterThanOrEqual(1)
  expect(taskTransports.transports[0].Reqtext).toBeTruthy()

  expect(Array.isArray(taskTransports.locations)).toBe(true)
  expect(taskTransports.locations.length).toBeGreaterThanOrEqual(1)
  expect(taskTransports.locations[0].TargetSysid).toBeTruthy()

  expect(Array.isArray(taskTransports.custFieldValues)).toBe(true)
  expect(taskTransports.custFieldValues.length).toBeGreaterThanOrEqual(1)
  expect(taskTransports.custFieldValues[0].Fieldid).toBeTruthy()
  expect(taskTransports.custFieldValues[0].Caption).toBeTruthy()
  expect(taskTransports.custFieldValues[0].Value).toBeTruthy()
})

test("Read a task with no transports", async () => {
  const client = createClient()

  const payload: ReadTaskTransportsInputs = {
    XTaskid: readEnv("AC_EMPTYTASK_ID")
  }

  const taskTransports = await client.readTaskTransports(payload)
  expect(Array.isArray(taskTransports.taskIds)).toBe(true)
  expect(taskTransports.taskIds).toHaveLength(1)
  expect(taskTransports.taskIds[0].Id).toBe(payload.XTaskid)

  expect(Array.isArray(taskTransports.transports)).toBe(true)
  expect(taskTransports.transports).toHaveLength(0)

  expect(Array.isArray(taskTransports.locations)).toBe(true)
  expect(taskTransports.locations).toHaveLength(0)

  expect(Array.isArray(taskTransports.custFieldValues)).toBe(true)
  expect(taskTransports.custFieldValues).toHaveLength(0)
})

test("Non-existing task", async () => {
  const client = createClient()

  const payload: ReadTaskTransportsInputs = {
    XTaskReference: "node red invalid"
  }

  return client
    .readTaskTransports(payload)
    .then(() => {
      fail("Error expected")
    })
    .catch(error => {
      expect(error.message).toBe("Task not found")
    })
})

test("Wrong parameters", async () => {
  const client = createClient()

  return client
    .readTaskTransports({})
    .then(() => {
      fail("Error expected")
    })
    .catch(error => {
      expect(error.message).toBe("No Task ID or Task Reference supplied")
    })
})

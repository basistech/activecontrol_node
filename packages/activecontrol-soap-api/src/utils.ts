import {
  composeXml,
  formatAsXml,
  formatAsXmlUpcase,
  xmlArray,
  xmlSubNode
} from "./soap"
import {
  CustomFields,
  isReturnMessage,
  isTask,
  messageIsError,
  ReadTaskTransportsInputs,
  ReadTaskTransportsResponse,
  TargetLocation,
  Task,
  TaskChangeData,
  Testers
} from "./taskDefinitions"
const SORTEDTASKCREATEBASEFIELDS: string[] = [
  "Id",
  "Caption",
  "Reference",
  "Groupid",
  "Typeid",
  "Testerid",
  "Priority",
  "Projectid",
  "Locked",
  "Path",
  "StatDepl",
  "StatPlan",
  "StatDeplMan",
  "StatPlanMan",
  "Owner",
  "Text"
]
const SORTEDTASKCREATECF = [
  "Cf500",
  "Cf501",
  "Cf502",
  "Cf503",
  "Cf504",
  "Cf505",
  "Cf506",
  "Cf507",
  "Cf508",
  "Cf509",
  "Cf510",
  "Cf511",
  "Cf512",
  "Cf513",
  "Cf514",
  "Cf515",
  "Cf516",
  "Cf517",
  "Cf518",
  "Cf519",
  "Cf520",
  "Cf521",
  "Cf522",
  "Cf523",
  "Cf524",
  "Cf525",
  "Cf526",
  "Cf527",
  "Cf528",
  "Cf529",
  "Cf530",
  "Cf531",
  "Cf532",
  "Cf533",
  "Cf534",
  "Cf535",
  "Cf536",
  "Cf537",
  "Cf538",
  "Cf539",
  "Cf540",
  "Cf541",
  "Cf542",
  "Cf543",
  "Cf544",
  "Cf545",
  "Cf546",
  "Cf547",
  "Cf548",
  "Cf549",
  "Cf550"
]
const SORTEDTASKCREATEFIELDS = [
  ...SORTEDTASKCREATEBASEFIELDS,
  ...SORTEDTASKCREATECF
]
const SORTEDTASKREADCHANGEFIELDS = [
  ...SORTEDTASKCREATEBASEFIELDS.map(x => x.toUpperCase()),
  ...SORTEDTASKCREATECF.map(x => `CF_${x.substr(2)}`)
]

const formatCf = (f: CustomFields[] | undefined) =>
  (f || [])
    .map(f => `<item><Id>${f.Id}</Id><Value>${f.Value}</Value></item>`)
    .join("\n")

const formatTesters = (t: Testers[] | undefined) =>
  (t || [])
    .map(
      t =>
        `<item>${formatAsXml(
          [
            "Testerid",
            "Targetroleid",
            "Targetid",
            "Systemid",
            "Mandt",
            "SmtpAddr",
            "Testername"
          ],
          t
        )}</item>`
    )
    .join("\n")

export const taskCreateToXml = (XSystemNumber: string, task: Partial<Task>) => {
  const XTask = formatAsXmlUpcase(SORTEDTASKCREATEFIELDS, task)
  const XtCustfields = formatCf(task.customFields)
  const XtTesters = formatTesters(task.testers)
  const XDescription = task.description

  return composeXml(
    ["XDescription", "XSystemNumber", "XTask", "XtCustfields", "XtTesters"],
    {
      XDescription,
      XSystemNumber,
      XTask,
      XtCustfields,
      XtTesters
    }
  )
}

const numbertoString = (o: any) => {
  const stringified = { ...o }
  for (const k of Object.keys(o))
    if (typeof o[k] === "number") stringified[k] = `${o[k]}`
  return stringified
}

const numbertoStringA = (a: any) => {
  if (Array.isArray(a)) return a.map(numbertoString)
  return a
}

export const taskfromXml = (main: any) => {
  const task = numbertoString(xmlSubNode(main, "YTask"))
  task.description = numbertoStringA(xmlSubNode(main, "YDescription"))
  task.customFields = numbertoStringA(xmlArray(main, "YtCustfields", "item"))
  task.testers = numbertoStringA(xmlArray(main, "YtTesters", "item"))
  // task has type any
  if (isTask(task)) return task // task is now of type Task
  throw new Error("Response is not a valid task") // task is not of type Task
}

export const taskChangeToXml = (
  XSystemNumber: string,
  task: Partial<TaskChangeData>
) => {
  if (!(task.XTask?.REFERENCE || task.XTask?.ID))
    throw new Error("Not a valid task change request")
  // typescript can't validate this but will hold due to values in constant
  const taskKeys = SORTEDTASKREADCHANGEFIELDS as (keyof Task)[]
  const XTask = formatAsXml(taskKeys, task.XTask)

  const XtCustfields = formatCf(task.XTask.customFields)
  const XtTesters = formatTesters(task.XTask.testers)
  const XDescription = task.XTask.description

  return composeXml(
    [
      "XDescription",
      "XSystemNumber",
      "XTask",
      "XtCustfields",
      "XtTesters",
      "XUpdateCustfields",
      "XUpdateDesc",
      "XUpdateTesters",
      "XUpddateTask"
    ],
    {
      XDescription,
      XSystemNumber,
      XTask,
      XtCustfields,
      XtTesters,
      XUpdateCustfields: task.XUpdateCustfields ? "X" : "",
      XUpdateDesc: task.XUpdateDesc ? "X" : "",
      XUpdateTesters: task.XUpdateTesters ? "X" : "",
      XUpddateTask: task.XUpddateTask ? "X" : ""
    }
  )
}

export const readTaskTransportsToXml = (
  inputs: ReadTaskTransportsInputs
): string => {
  if (inputs.XTaskid) {
    return composeXml(["XTaskid"], { XTaskid: inputs.XTaskid })
  } else {
    return composeXml(["XTaskReference"], {
      XTaskReference: inputs.XTaskReference
    })
  }
}

export const readTaskTransportsFromXml = (
  response: any
): ReadTaskTransportsResponse => {
  const isValidResponse = (resp: any): resp is ReadTaskTransportsResponse =>
    Array.isArray(resp.taskIds) && resp.taskIds.length > 0

  const message = xmlSubNode(response, "YReturn")

  if (!isReturnMessage(message)) throw new Error("Invalid server response")

  if (messageIsError(message)) throw new Error(message.Message)

  const taskTransports: ReadTaskTransportsResponse = {
    custFieldValues: xmlArray(response, "YtCustfieldValues", "item"),
    locations: xmlArray(response, "YtLocations", "item"),
    taskIds: xmlArray(response, "YtTaskIds", "item"),
    transports: xmlArray(response, "YtTransports", "item").map(x => ({
      ...x,
      Completed: x.Completed === "X"
    }))
  }

  if (!isValidResponse(taskTransports))
    throw new Error("Invalid server response")

  return taskTransports
}

export const base64decode = (x: string) =>
  Buffer.from(x, "base64").toString("utf-8")

export const isValidSystemNumber = (x: string): boolean => !!x?.match(/^\d\d$/)
export const isValidTarget = (x: string): boolean => !!x?.match(/^\d\d\d\d$/)
export const isValidApprLocation = (x: string): boolean => !!x?.match(/^[IO]$/)
export const isValidReference = (x: string): boolean =>
  x?.length > 0 && x?.length <= 20
export const isValidId = (x: string): boolean =>
  x?.length === 20 && !!x?.match(/^\d+$/)
export const isValidRequest = (x: string): boolean =>
  x?.length === 10 && x?.[3] === "K"
export const isValidLocation = (x: string): x is TargetLocation =>
  x?.length === 1 && !!x?.match(/[IQTO]/)

import { isValidId, isValidReference } from "./utils"

// Reusable Task structure
export interface Task {
  ID: string
  CAPTION: string
  REFERENCE: string
  GROUPID: string
  TYPEID: string
  TESTERID: string
  PRIORITY: string
  PROJECTID: string
  LOCKED: string
  PATH: string
  STAT_DEPL: string
  STAT_PLAN: string
  STAT_DEPL_MAN: string
  STAT_PLAN_MAN: string
  OWNER: string
  TEXT: string
  CF_500: string
  CF_501: string
  CF_502: string
  CF_503: string
  CF_504: string
  CF_505: string
  CF_506: string
  CF_507: string
  CF_508: string
  CF_509: string
  CF_510: string
  CF_511: string
  CF_512: string
  CF_513: string
  CF_514: string
  CF_515: string
  CF_516: string
  CF_517: string
  CF_518: string
  CF_519: string
  CF_520: string
  CF_521: string
  CF_522: string
  CF_523: string
  CF_524: string
  CF_525: string
  CF_526: string
  CF_527: string
  CF_528: string
  CF_529: string
  CF_530: string
  CF_531: string
  CF_532: string
  CF_533: string
  CF_534: string
  CF_535: string
  CF_536: string
  CF_537: string
  CF_538: string
  CF_539: string
  CF_540: string
  CF_541: string
  CF_542: string
  CF_543: string
  CF_544: string
  CF_545: string
  CF_546: string
  CF_547: string
  CF_548: string
  CF_549: string
  CF_550: string
  description: string
  customFields: CustomFields[]
  testers: Testers[]
}

export interface TaskChangeData {
  XTask: Partial<Task>
  XUpdateCustfields: boolean
  XUpdateDesc: boolean
  XUpdateTesters: boolean
  XUpddateTask: boolean
}
export interface CustomFields {
  Id: string
  Value: string
}

export interface Testers {
  Testerid: string
  Targetroleid: string
  Targetid: string
  Systemid: string
  Mandt: string
  SmtpAddr: string
  Testername: string
}

export interface ReturnMessage {
  Msgtyp: "S" | "I" | "W" | "E" | "A" | "X" | ""
  Msgid: string
  Msgnum: string
  Message: string
  Msgv1: string
  Msgv2: string
  Msgv3: string
  Msgv4: string
  Exception: string
}

export enum TestResultCodes {
  Passed = "0",
  ProblemFound = "1",
  Information = "2",
  Waiting = "3",
  BypassTesting = "4"
}

export enum TargetLocation {
  Inbox = "I",
  TestQueue = "T",
  ImportQueue = "Q",
  Outbox = "O"
}

export interface TestResultsInputs {
  XClose: boolean
  XComment: string
  XRescode: TestResultCodes
  XTarget: string
  XTaskid: string
}

export interface ReadTaskTransportsInputs {
  XTaskid?: string
  XTaskReference?: string
}

export type ReadTaskTransportsCustfield = {
  Trkorr: string
  Fieldid: string
  Caption: string
  Value: string
}

export type ReadTaskTransportsLocation = {
  Trkorr: string
  Target: string
  TargetSysid: string
  Location: TargetLocation
}

export type ReadTaskTransportsList = {
  Trkorr: string
  Reqtext: string
  Trstatus: string
  Trfunction: string
  Groupid: string
  GroupDesc: string
  Typeid: string
  TypeDesc: string
  Requestor: string
  Reqdate: string
  Reqtime: string
  Reldate: string
  Reltime: string
  Completed: boolean
  LastImportDate: string
  LastImportTime: string
  LastImportTargetId: string
  LastImportTargetDesc: string
  Path: string
}

export interface ReadTaskTransportsResponse {
  custFieldValues: ReadTaskTransportsCustfield[]
  locations: ReadTaskTransportsLocation[]
  taskIds: { Id: string }[]
  transports: ReadTaskTransportsList[]
}

export interface ReadTaskInputs {
  XTaskid: string
}

export function validateTestResultsInputs(inputs: TestResultsInputs) {
  if (!inputs.XRescode?.match(/^\d$/))
    throw new Error("Invalid test result code, should be a single digit number")
  if (!inputs.XTaskid?.match(/^[\d]+$/)) throw new Error("Invalid Task id")
  if (!inputs.XTarget?.match(/^\d\d\d\d$/)) throw new Error("Invalid Target ID")
}

// type guard - assumes an object with a numeric ID and a CF_550 field to be a task
export const isTask = (x: any): x is Task =>
  !!`${x?.ID}`.match(/^\d+$/) && "CF_550" in x

export const isReturnMessage = (x: any): x is ReturnMessage =>
  (x?.Msgtyp === "" && "Message" in x && "Msgv1" in x) ||
  (!!`${x?.Msgtyp}`.match(/^[SIWEAX]$/) && !!x?.Message && "Msgv1" in x)

export const messageIsError = (m: ReturnMessage) => !!m.Msgtyp.match(/[EXA]/)

export function validateReadTaskInputs(inputs: ReadTaskInputs): void {
  if (!isValidId(inputs.XTaskid)) throw new Error("Bad Task ID supplied")
}

export function validateReadTaskTransportsInputs(
  inputs: ReadTaskTransportsInputs
): void {
  if (!inputs.XTaskid && !inputs.XTaskReference)
    throw new Error("No Task ID or Task Reference supplied")

  if (inputs.XTaskid && !isValidId(inputs.XTaskid))
    throw new Error("Bad Task ID supplied")

  if (inputs.XTaskReference && !isValidReference(inputs.XTaskReference))
    throw new Error("Bad Task Reference supplied")
}

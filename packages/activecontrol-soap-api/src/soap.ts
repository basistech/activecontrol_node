import { parse } from "fast-xml-parser"

export function xmlSubNode(root: any, ...path: string[]) {
  let current = root
  for (const field of path)
    if (!current) return
    else current = current[field]
  return current
}
export function xmlArray(root: any, ...path: string[]) {
  const r = xmlSubNode(root, ...path)
  if (!r) return []
  return Array.isArray(r) ? r : [r]
}
export const stripEnvelope = (rawsoap: any, ...nodes: string[]) =>
  xmlSubNode(rawsoap, "soap-env:Envelope", "soap-env:Body", ...nodes) ||
  xmlSubNode(rawsoap, "soapenv:Envelope", "soapenv:Body", ...nodes)

export const wrapInEnvelope = (rootNode: string, payload: string) =>
  `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:soap:functions:mc-style">
        <soapenv:Header/>
        <soapenv:Body>
           <${rootNode}>
              ${payload}
           </${rootNode}>
        </soapenv:Body>
     </soapenv:Envelope>`

const formatXmlString = (x: any) =>
  x
    ?.toString()
    ?.replace(/&/g, "&amp;")
    ?.replace(/</g, "&lt;")
    ?.replace(/>/g, "&gt;")
    ?.replace(/"/g, "&quot;") || ""

export const composeXml = <T>(fields: (keyof T)[], record: T): string =>
  fields.map(n => `<${n}>${record[n]}</${n}>`).join("\n")

export const formatAsXml = <T>(fields: (keyof T)[], record: Partial<T>) =>
  fields.map(n => `<${n}>${formatXmlString(record[n])}</${n}>`).join("\n")

/**
 * type checking is broken as task has uppercase keys, but its XML version uses camelcase
 * Also, custom fields are CF_xxx in READ/CHANGE/JSON but Cfxxx in create
 */
export const formatAsXmlUpcase = <T>(fields: string[], record: T) => {
  const toKey = (k: string) =>
    (k.match(/cf\d\d\d/i) ? `CF_${k.substr(2)}` : k.toUpperCase()) as keyof T // true for task fields
  return fields
    .map(n => `<${n}>${formatXmlString(record[toKey(n)])}</${n}>`)
    .join("\n")
}

export const extractErrorMessage = (error: any) => {
  // try to extract a SOAP error from the message body
  const xmlMessage = xmlSubNode(
    stripEnvelope(parse(`${error?.response?.data}`)),
    "soap-env:Fault",
    "faultstring"
  )
  if (xmlMessage) return xmlMessage
  if (error?.message) return error.message
  if (error?.toString) return error.toString()
  return "unknown error"
}
